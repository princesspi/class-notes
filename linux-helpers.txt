show listening ports
    sudo ss -tupln

disable ping
    sudo nano /etc/ufw/before.rulers
    #ok icmp codes for INPUT
    -A ufw-before-input -p icmp --icmp-type echo-request -j DROP
echo $? = show last command exit code