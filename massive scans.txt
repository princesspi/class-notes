##masscan
   scanning the entire internet: https://www.youtube.com/watch?v=7XMIFTRiAGA
#github
https://github.com/robertdavidgraham/masscan

#man page
https://manpages.ubuntu.com/manpages/xenial/man8/masscan.8.html

#install
sudo apt install libpcap0.8-dev libuv1-dev masscan

#run with file
masscan -c masscan.conf
#---masscan.conf---#
# targets
#range = 10.0.0.0/8,192.168.0.0/16
#range = 172.16.0.0/14
range = 10.0.0.0/24
#ports = 20-25,80,U:53
ports = 22,80,443
ping = true
# adapter
#adapter = eth0
adapter = wlan0
#adapter-ip = 192.168.0.1
#router-mac = 66-55-44-33-22-11
# other
#exclude-file = /etc/masscan/exludes.txt
#--- ---#

massscan --exclude exclude.txt
#scan local subnet
sudo masscan 10.0.0.0/24 -p80,22,443 --open-only --rate 25000 --adapter wlan0 -oL massout.txt
-oX = xml output

Sharding
masscan 0.0.0.0/0 -p0-65535 --shard 1/3
# masscan 0.0.0.0/0 -p0-65535 --shard 2/3
# masscan 0.0.0.0/0 -p0-65535 --shard 3/3

Example of output in binary format
masscan 192.185.117.0/24 -p 3306,21 --rate 9999999 --output-format binary --output-filename out.b
in

#scan the entire ipv4 internet for all RCP ports
masscan 0.0.0.0/0 -p0-65535

#rate
--rate 200000000 #maxes out network adaptor (can go up if not maxed!)
use with nload in byobu to monitor speed!

##nmap
below is way, way, way slower than masscan, to the order of 256 every five minutes (1000 ports each). spits out raw receive data lines and verbose lines
nmap range/suffix -sT -sV -Pn -n -vvv --min-rate=5000 --min-hostgroup=256 --min-parallelism=256
   -sT = tcp
   -sV = give headers
   -Pn = dont ping
   -n = never do dns resolution
   --man-rate=5000  = send minimum of 5000 packets a second
   -vvv = very very very verbose
   -min-hostgroup=256 = work on 256 hosts at a time

// test
nmap -iL ips.txt -sT -sV -n -vvv --min-rate=5000 --min-hostgroup=256 
   --min-paralellism=256 = run 256 threads

distribute nmap workloads
needs tasa: https://github.com/PaulMcMillan/tasa
tnmap: https://github.com/PaulMcMillan/tasa/blob/master/examples/tnmap.py
python tnmap.py <ip>/<suffix> | xargs -I CMD -P 100 = run 100 threads of tnmap.py

xargs
   -I CMD = put each line into variable CMD
   -P 100 = 100 processes
command with tnmap for redis: // work on later

cat ips1337.txt | xargs -I IP -P 24 nmap -p1337 -sT --append-output -oN bundle_of_scalpels.txt -Pn IP

python tnmap.py <ip>/<suffix> xargs -I CMD -P 100 nmap -sT -sV -sC -n -vvvv -oX --min-rate=1000 --min-hostgroup=256 --min-paralellusm=256 - CMD