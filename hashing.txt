hashing
sha256 [file] > hashes.txt
sha256 -c [file_hashes] 

powershell: get-filehash <file_in> [-algorithm md5/etc] //defaults to SHA256

===PHP===
/* Password. */
$password = 'my secret password';

/** Legacy Hashing **/
/* Set the "cost" parameter to 12. */
$options = ['cost' => 12];
$hash = password_hash($password, PASSWORD_DEFAULT, $options);

/* if supported is better 7.3.0+ */
$options = ['memory_cost' => 2048, 'time_cost' => 4, 'threads' => 3]
$hash = password_hash($password, PASSWORD_ARGON2ID, $options);
/* Verify pass */
password_verify($password, $hash_stored)