# Get Plugged Into Cyber Security Social Media
## Cyber Security Links

### Reddit
https://www.reddit.com/r/redteamsec/<br>
https://www.reddit.com/r/Defcon/<br>
https://www.reddit.com/r/blueteamsec/<br>
https://www.reddit.com/r/masterhacker/ (jokes)<br>
https://www.reddit.com/r/oscp/<br>
https://www.reddit.com/r/HowToHack/<br>
https://www.reddit.com/r/cybersecurity/<br>
https://www.reddit.com/r/codes/ (somewhat related)<br>
https://www.reddit.com/r/TOR/<br>
https://www.reddit.com/r/cryptography/<br>
https://www.reddit.com/r/OSINT/<br>
https://www.reddit.com/r/netsec/<br>
<br>
### Youtube
https://www.youtube.com/c/hak5<br>
https://www.youtube.com/user/Hak5Darren<br>
https://www.youtube.com/c/ShannonMorse<br>
https://www.youtube.com/user/DeviantOllam<br>
https://www.youtube.com/c/MentalOutlaw<br>
https://www.youtube.com/c/NetworkChuck<br>
https://www.youtube.com/c/Seytonic<br>
https://www.youtube.com/user/Computerphile<br>
https://www.youtube.com/channel/UCPiN9NPjIer8Do9gUFxKv7A<br>
https://www.youtube.com/channel/UCQSpnDG3YsFNf5-qHocF-WQ<br>
https://www.youtube.com/channel/UCTbTMfVyCfs9p8SPsi3xEZQ<br>
https://www.youtube.com/channel/UCP7WmQ_U4GB3K51Od9QvM0w<br>
https://www.youtube.com/c/JohnHammond010<br>
https://www.youtube.com/c/IamJakoby<br>
https://www.youtube.com/channel/UCz-Z-d2VPQXHGkch0-_KovA<br>
https://www.youtube.com/c/HackerSploit<br>
https://www.youtube.com/c/TheXSSrat<br>
https://www.youtube.com/c/ItproTv<br>
https://www.youtube.com/c/InfoSecInstitute<br>
https://www.youtube.com/channel/UCp1rLlh9AQN9Pejzbg9dcAg<br>

### Twitter
https://twitter.com/CyberSecurityN8<br>
https://twitter.com/threatshub<br>
https://twitter.com/i/topics/898650876658634752<br>
https://twitter.com/i/lists/1420237973350723586<br>
https://twitter.com/three_cube<br>
https://twitter.com/HackingTutors<br>
https://twitter.com/IntelTechniques<br>
https://twitter.com/xtremepentest<br>
https://twitter.com/i/topics/898673391980261376 (web development)<br>
https://twitter.com/the404code<br>
https://twitter.com/i/topics/1047123725525479425<br>
https://twitter.com/CyberIQs_<br>
https://twitter.com/Cybersecinsider<br>
https://twitter.com/Free_AI_Books<br>
https://twitter.com/WebBreacher<br>
https://twitter.com/technisette<br>
https://twitter.com/davidbombal<br>
https://twitter.com/technisette<br>
https://twitter.com/Alh4zr3d<br>
https://twitter.com/theXSSrat<br>
https://twitter.com/atomiczsec<br>
https://twitter.com/I_Am_Jakoby<br>
https://twitter.com/CosmodiumC<br>
<br>
### Websites
https://www.cosmodiumcs.com/<br>
https://arstechnica.com/<br>
https://www.bleepingcomputer.com/<br>
https://www.exploit-db.com/<br>
https://www.nativeintelligence.com/resources/cyber-security-links/<br>
http://virusscan.jotti.org<br>
https://www.cybersecurity-review.com/useful-links/<br>
https://www.montreat.edu/cybersecurity/resources/<br>
https://thehackernews.com/<br>
https://hak5.org/<br>
https://krebsonsecurity.com/<br>
https://isc.sans.edu/<br>
https://isc.sans.edu/links.html<br>
https://www.securityweek.com/<br>
https://blog.detectify.com/<br>
https://learn.microsoft.com/en-us/training/paths/active-directory-domain-services/<br>
https://school.infosec4tc.com/courses/category/Free<br>