dig <domain> = domain info
dig axfr @ns.name.server.com domain.com
whois <domain> = whois info
nslookup --type=all <domain>